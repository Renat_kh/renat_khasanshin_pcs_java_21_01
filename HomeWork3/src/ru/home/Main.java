package ru.home;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.print("Ввидите текст: ");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();


        Map<Character, Integer> map = new HashMap<>();
        for (char symbol : str.toCharArray()) {
            if (!map.containsKey(symbol)) {
                map.put(symbol, 1);
            }
            //  при совпадение, get + 1
            else {
                map.replace(symbol, map.get(symbol) + 1);
            }
        }

        for (char symbol : map.keySet()) {
            System.out.println("Символ:\"" + symbol + "\" - встречается кол-во раз = " + map.get(symbol));
        }
    }
}