package ru.home;

public class Program {
    private String name;
    private int ages;
    private String err = "\u001B[31mУкажите возрастные ограничения\u001B[0m - ";

    public Program(String name, int ages) {
        this.name = name;
        this.ages = ages;
    }
	
    public void showNamesPrograms () {
        if (ages < 18) {
            System.out.println(getName());
        }
        else
            System.out.println("\u001B[31mВам нет 18 лет\u001B[0m - " + getName());
    }

    public String getName() {
        if (ages > 0)
            return name;
        else
            return err + name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAges(int ages) {
        this.ages = ages;
    }
}