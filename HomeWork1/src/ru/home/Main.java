package ru.home;

public class Main {

    public static void main(String[] args) {

        TV tv = new TV("TV GoldStar");
        RemoteController control = new RemoteController(tv);

        Channel c2x2 = new Channel("2x2");
        Channel youtube = new Channel("Youtube");

        Program p1 = new Program("",0);
        Program p2 = new Program("",0);

        Program p3 = new Program("",0);
        Program p4 = new Program("",0);
        Program p5 = new Program("",0);

        Settings settings = new Settings();
        settings.settingsPrograms(p1, p2, p3, p4, p5);

        addProgram( p1, p2, p3, p4, p5, c2x2, youtube);
        addChannel(c2x2, youtube, tv);

        Printer printer = new Printer();
        printer.printTV(tv);
        printer.printRemoteController(control);
    }

    static void addChannel(Channel c2x2, Channel youtube, TV tv) {
        tv.addChannel(c2x2);
        tv.addChannel(youtube);
    }

    static void addProgram(Program p1, Program p2, Program p3, Program p4, Program p5, Channel c2x2, Channel youtube) {
       c2x2.addProgram(p1);
       c2x2.addProgram(p2);
       youtube.addProgram(p3);
       youtube.addProgram(p4);
       youtube.addProgram(p5);
    }
}
