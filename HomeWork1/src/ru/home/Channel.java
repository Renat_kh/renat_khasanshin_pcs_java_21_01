package ru.home;

import java.util.Random;

public class Channel {
    private static final int MAX_PROGRAMS_COUNT = 7;
    private String name;
    private Program[] programs;
    private int programsCount;

    private Random random;

    public Channel(String name) {
        this.name = name;
        this.programsCount = 0;
        this.programs = new Program[MAX_PROGRAMS_COUNT];
        this.random = new Random();
    }

    public void addProgram(Program program) {
            if (programsCount < MAX_PROGRAMS_COUNT) {
            programs[programsCount] = program;
            programsCount++;
            }
    }

    public void showProgram() {
        System.out.print("Канал " + name + " программа - ");
        int programNumber = random.nextInt(programsCount);
        programs[programNumber].showNamesPrograms();
    }

}
