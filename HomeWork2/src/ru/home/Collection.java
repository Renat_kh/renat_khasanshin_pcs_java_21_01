package ru.home;

public interface Collection<B> {

    void add(B element);

    boolean contains(B element);

    int size();


    void remove(B element);

    Iterator iterator();
}
