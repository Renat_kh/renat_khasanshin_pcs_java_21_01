package ru.home;

public interface List<C> extends Collection<C> {

    C get(int index);
    void addFirst(C element);
    void removeByIndex(int index);
    int indexOf(C element);
    void remove(C element);
}
