package ru.home;

public class ArrayList<D> implements List<D> {
    private static final int DEFAULT_SIZE = 10;
    // ссылка на массив с элементами
    private  D[] elements;

    private int size;

    public ArrayList() {
        this.elements = (D[])new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    @Override
    public D get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            System.err.println("Вышли за границы");
            return null;

        }
    }

    @Override
    public void addFirst(D element) {
        if (size == elements.length) {
            resize();
        }
        for (int i = size + 1; i > 0; i--) {
            this.elements[i] = this.elements[i - 1];
        }

        this.elements[0] = element;
        size++;
    }

    @Override
    public void add(D element) {
        // если элементов стало столько, сколько в принципе может вместиться в этот массив
        if (size == elements.length) {
            resize();
        }

        this.elements[size] = element;
        size++;
    }

    private void resize() {
        D[] newElements = (D[])new Object[elements.length + elements.length / 2];
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    @Override
    public boolean contains(D element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(D element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void remove(D element) {
        removeByIndex(indexOf(element));
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < size){
            for (int i = index; i < size; i++){
                elements[index] = elements[index + 1];
                index++;
            }
        }
        size--;
    }


    private class ArrayListIterator implements Iterator<D> {

        private int current = 0;

        @Override
        public D next() {
            D element = get(current);
            current++;
            return element;
        }

        public ArrayListIterator() {
        }

        @Override
        public boolean hasNext() {
            return current < size;
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
