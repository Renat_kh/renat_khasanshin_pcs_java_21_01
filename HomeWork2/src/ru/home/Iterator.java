package ru.home;

public interface Iterator<A> {

    A next();

    boolean hasNext();
}
